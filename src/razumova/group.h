#ifndef GROUP_H
#define GROUP_H

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include "student.h"

using namespace std;

class Student;
class Group
{
public:
    Group();
    Group(const Group &gr);
    Group(std::string title, int course);
    void setStudents(vector<Student> s);
    void addStudent(Student *st);
    int findStudent(Student &st);
    int findStudent(int id);
    int findStudent(std::string fio);
    void removeStud(int ind);
    void setHead(int ind);
	void changeHead();
    Student* getHead();
	void	setMarks(int n); // n - ���������� ������ ��� ������� ��������
    double getAvMark();
    void setTitle(std::string title);
	string getTitle();
    void print();
	int getDewager();
	int getStudentsNum();
	Student * bestStudent();
private:
    vector<Student*> students;
    Student *head;
    std::string title;
    int course;


    //int count;
};

#endif // GROUP_H

