#include <iostream>
#include <string>
#include <fstream>
#include <cmath>

using namespace std;

#include "Student.h"
#include "Group.h"
#include "Dekanat.h"

Dekanat::Dekanat() {
	students = 0;
	groups = 0;
	numbS = 0;
	numbG = 0;
}

Dekanat::~Dekanat() {
	delete[] students;
	delete[] groups;
}

void Dekanat::readGroup(string file) {
	string groupName;
	int i = 0;
	ifstream f(file);
	while (getline(f, groupName)) {
        numbG++;
    }
	f.clear();
	f.seekg(0);
	groups = new Group *[numbG];
	while (getline(f, groupName)) {
        groups[i++] = new Group(groupName);
    }
	f.close();
}

bool Dekanat::groupsInfo() {
	if ( numbG == 0 )
	{
		cout << "No groups." << endl;
		return false;
	}
	else
	{
		for ( int i = 0; i < numbG; i++ )
			groups[i]->info();
		return true;

	}
}

void Dekanat::readStudents(string file) {
	int i = 0;
	int id = 0;
	string fio;
	string groupName;
	string studentFio;
	ifstream f(file);
	while (getline(f, studentFio)) {
        numbS++;
    }
	f.clear();
	f.seekg(0);
	students = new Student *[numbS];
	while (getline(f, studentFio)) {
        separateString(studentFio, id, fio, groupName);
		students[i] = new Student(id, fio);
		searchGroup(groupName)->addStudent(students[i++]);
		id = 0;
    }
	f.close();
}

bool Dekanat::studentsInfo() {
	if ( numbS == 0 )
	{
		cout << "No students." << endl;
		return false;
	}
	else
	{
		for ( int i = 0; i < numbS; i++ )
			students[i]->info();
		return true;

	}
}

void Dekanat::writeStudents(string file) {
	ofstream f(file);
	for ( int i = 0; i < numbS; i++ )
		f << students[i]->getId() << " " << students[i]->getFio() << " " << students[i]->getGroup()->getTitle() << endl;
	f.close();
}

Group* Dekanat::searchGroup(string title) {
	for ( int i = 0; i < numbG; i++)
		if (groups[i]->getTitle() == title)
			return groups[i];
	return 0;
}

void Dekanat::randomMarks(int number) {
	for ( int i = 0; i < numbS; i++ )
		for ( int j = 0; j < number; j++ )
			students[i]->addMark(rand()%5 + 1);
}

bool Dekanat::transferStudent(int id, string groupName) {
	for ( int i = 0; i < numbS; i++ )
		if ( students[i]->getId() == id )
		{
			students[i]->getGroup()->delStudent(id);
			for ( int j = 0; j < numbG; j++ )
				if ( groups[j]->getTitle() == groupName )
				{
					groups[j]->addStudent(students[i]);
					return true;
				}
		}
	return false;
}

void Dekanat::stats() {
	float sum = 0;
	float av = 0;
	int max = 0;
	int maxInd = 0;
	for ( int i = 0; i < numbG; i++ )
	{
		av = groups[i]->averageGrMark();
		sum += av;
		if ( av > max )
		{
			max = av;
			maxInd = i;
		}
	}
	cout << "Statistics: " << endl;
	cout << "Average mark of groups: " << sum / numbG << endl;
	cout << "Best group: " << groups[maxInd]->getTitle() << endl;
	max = 0;
	maxInd = 0;
	for ( int j = 0; j < numbS; j++ )
		if ( students[j]->averageMark() > max )
		{
			max = students[j]->averageMark();
			maxInd = j;
		}
	cout << "Best student: " << endl;
	students[maxInd]->info();
}

bool Dekanat::delStudent(int id) {
	for ( int i = 0; i < numbS; i++ )
		if ( students[i]->getId() == id )
		{
			students[i] = students[numbS-- - 1];
			return true;
		}
	return false;
}

void Dekanat::dismissalStudents(float min) {
	for ( int i = 0; i < numbS; i++ )
		if ( students[i]->averageMark() < min )
		{
			students[i]->getGroup()->delStudent(students[i]->getId());
			delStudent(students[i]->getId());
		}
}

void Dekanat::setRandHeads() {
	for ( int i = 0; i < numbG; i++ )
		groups[i]->setRandHead();
}

void separateString(string str, int &id, string &fio, string &groupName) { //���������� ������ �� �����
	int i = 0;
	while (str[i] != ' ') id = (id * 10) + (str[i++] - '0');
	int bgnFio = i + 1;
	while (str[i] < '0' || str[i] > '9') i++;
	fio = str.substr(bgnFio, i - 1 - bgnFio);
	groupName = str.substr(i);
}