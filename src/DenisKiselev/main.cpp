#include <iostream>
#include <string>
#include <clocale>

using namespace std;

#include "Student.h"
#include "Group.h"
#include "Dekanat.h"

int main()
{
	setlocale(LC_CTYPE, "Russian");
	Dekanat dekan;
	dekan.readGroup("group.txt");
	dekan.readStudents("student.txt");
	dekan.randomMarks(15);		//����������� ��������� ������
	dekan.setRandHeads();		//��������� ������� �������
	dekan.groupsInfo();			//����� ���������� � �������
	dekan.transferStudent(43, "381603");		//������� �������� � ID 43 � ������ "381603"
	dekan.dismissalStudents(2.5);		//���������� ���������, � ������� ������� ���� ������ 2.5
	dekan.groupsInfo();
	dekan.stats();			//����������
	dekan.writeStudents("new_student.txt");
	return 0;
}