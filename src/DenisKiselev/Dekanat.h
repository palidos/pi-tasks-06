#pragma once

#include <string>
#include "Student.h"
#include "Group.h"

using namespace std;

class Dekanat {
private:
	Student **students;
	Group **groups;
	int numbS;
	int numbG;
public:
	Dekanat();
	~Dekanat();

	void readGroup(string file);
	bool groupsInfo();
	void readStudents(string file);
	bool studentsInfo();
	void writeStudents(string file);
	Group* searchGroup(string title);
	void randomMarks(int number);
	bool transferStudent(int id, string groupName);
	void stats();
	bool delStudent(int id);
	void dismissalStudents(float min);
	void setRandHeads();
};

void separateString(string str, int &id, string &fio, string &groupName);