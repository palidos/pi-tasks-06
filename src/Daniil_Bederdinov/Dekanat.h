#include "Student.h"
#include "Group.h"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;



class Dekanat
{
	vector<Group*> groups;
	vector<Student*> students;
public:
	Dekanat()
	{
		groups.reserve(3);
		students.reserve(30);
	}

	void LoadGroups()
	{
		ifstream fcin("Groups.txt");
		string title;
		while (fcin >> title)
			groups.push_back(new Group(title));
	}

	void LoadStud()
	{
		ifstream fscin("Students.txt");
		int id;
		string fio, f, i, o, title;
		while (fscin >> id)
		{
			fscin >> f >> i >> o;
			fio = f + " " + i + " " + o;
			fscin >> title;
			for (unsigned int i = 0; i < groups.size(); i++)
			{
				if (title == groups[i]->getTitle())
				{
					students.push_back(new Student(id, fio, groups[i]));
					groups[i]->addStud(students.back());
				}
			}
		}
	}
	void AddMark(int id, int mark)
	{
		FindStud(id)->AddMark(mark);
	}

	void TransferStud(int id, string gr_title)
	{
		Student* stud = FindStud(id);
		Group* gr = FindGroup(gr_title);
		if (stud == NULL)
		{
			cout <<"Student id# "<<id<< " doesn't exist" << endl;
			return;
		}
		if (gr == NULL)
		{
			cout << "Group "<< gr_title <<" doesn't exist" << endl;
			return;
		}
		DeleteStudGroup(stud);
		AddStud(stud, gr);
		cout << "Student id# " << id << " transfered in group: " << gr_title << endl;
	}

	Group* FindGroup(string title)
	{
		for (unsigned int i = 0; i < groups.size(); i++)
		{
			if (groups[i]->getTitle() == title)
				return groups[i];
		}
		return NULL;
	}

	Student* FindStud(int id)
	{
		for (unsigned int i = 0; i < students.size(); i++)
		{
			if (students[i]->GetId() == id)
				return students[i];
		}
		return NULL;
	}

	void AddStud(Student* stud, Group* gr) {
		gr->addStud(stud);
		stud->SetGroup(gr);
	}

	void AddStud(Student* stud, string gr_title)
	{
		for (unsigned int i = 0; i < students.size(); i++)
		{
			if (students[i]->GetId() == stud->GetId()) 
			{
				cout << "Student id# " << stud->GetId() << " is already exists" << endl;
				return;
			}
		}
		Group* gr = FindGroup(gr_title);
		if (gr == NULL)
		{
			cout << "Group " << gr_title << " doesn't exist" << endl;
			return;
		}
		stud->SetGroup(gr);
		students.push_back(stud);
		gr->addStud(stud);
		cout << "Student id# " << stud->GetId() << " added in group " << gr_title << endl;
	}

	void DeleteStudGroup(Student * stud)
	{
		Group* gr = stud->GetGroup();
		if (gr->GetHead() == stud)
			gr->SetHead(NULL);
		stud->SetGroup(NULL);
		gr->DeleteStudent(stud);
	}

	void DeleteStud(int id)
	{
		Student* stud = FindStud(id);
		if (stud == NULL)
		{
			cout << id << " - Student doesn't exist" << endl;
			return;
		}
		DeleteStudGroup(stud);
		for (unsigned int i = 0; i < students.size(); i++)
		{
			if (students[i] == stud)
			{
				students[i] = students.back();
				students.resize(students.size() - 1);
				break;
			}
		}
		cout << "Deleted student - " << id << endl;
	}

	void AddHead(string title, int id)
	{
		Group* gr = FindGroup(title);
		Student* stud = FindStud(id);
		if (stud == NULL)
		{
			cout << id << " - Student doesn't exist" << endl;
			return;
		}
		if (gr == NULL)
		{
			cout << title << " - Group doesn't exist" << endl;
			return;
		}
		if (stud->GetGroup() != gr)
		{
			cout << "There is no " << id << " student in group " << title << endl;
			return;
		}
		gr->SetHead(stud);
		cout << "Student - " << id << " is now Head of the group " << title << endl;
	}

	void RandomMarks(int count)
	{
		for (unsigned int i = 0; i < students.size(); i++)
		{
			for (int j = 0; j < count; ++j)
			{
				int new_mark = rand() % 5 + 1;
				AddMark(students[i]->GetId(), new_mark);
			}
		}
	}

	Group* FindBestGroup()
	{
		Group* BestGr = groups[0];
		for (unsigned int i = 0; i < groups.size(); i++)
		{
			if (BestGr->AvgMark() < groups[i]->AvgMark())
				BestGr = groups[i];
		}
		cout << "Best group: " << BestGr->getTitle() << ". Averange mark: " << BestGr->AvgMark() << endl;
		return BestGr;
	}

	Student* FindBestStudent()
	{
		Student* BestStud = students[0];
		for (unsigned int i = 0; i < students.size(); i++)
		{
			if (BestStud->GetAvgMark() < students[i]->GetAvgMark())
				BestStud = students[i];
		}
		cout << "Best student id# " << BestStud->GetId() <<", from group: "<< BestStud->GetGroup()->getTitle() << ". Averange mark: " << BestStud->GetAvgMark() << endl;
		return BestStud;
	}

	void WriteStudents()
	{
		ofstream fout("UpdatedStudents.txt");
		for (unsigned int i = 0; i < students.size(); i++)
		{
			fout << students[i]->GetId() << " " << students[i]->GetFIO() << " " << students[i]->GetGroup()->getTitle() << endl;
		}
		fout.close();
	}

	void WriteGroups() 
	{
		ofstream fout("UpdatedGroups.txt");
		for (unsigned int i = 0; i < groups.size(); i++) 
		{
			fout << groups[i]->getTitle() << endl;
		}
		fout.close();
	}


	void start() {
		LoadGroups();
		LoadStud();
		TransferStud(43, "381303");
		TransferStud(7, "371605");
		TransferStud(80, "381608");;
		AddStud(new Student(1, "�������� ����� ��������"), "381608");
		AddStud(new Student(56, "�������� ������ ���������"), "381608-1");
		AddStud(new Student(56, "������� �������� ����������"), "381608");
		DeleteStud(59);
		DeleteStud(4);
		AddHead("381608-1", 3);
		AddHead("381608", 80);
		AddHead("381603", 1);
		AddHead("381303", 1);
		RandomMarks(5);
		FindBestGroup();
		FindBestStudent();
		WriteStudents();
		WriteGroups();
	}

};
