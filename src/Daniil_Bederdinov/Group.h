#ifndef GRP 
#define GRP
#include <iostream>
#include "Student.h"
using namespace std;

class Student;
class Group
{
private:
	string title;
	vector<Student*> st;
	Student *head;
public:
	Group(string title, Student* head)
	{
		this->title = title;
		this->head = head;
		st.reserve(50);
	}
	Group(string title)
	{
		this->title = title;
		st.reserve(30);
	}

	void addStud(Student* student)
	{
		st.push_back(student);
	}

	Student* FindStudent(int id) {
		for (unsigned int i = 0; i < st.size(); i++)
		{
			if (st[i]->GetId() == id)
				return st[i];
		}
		return NULL;
	}

	string getTitle()
	{
		return title;
	}

	void SetTitle(string title)
	{
		this->title = title;
	}

	void SetHead(Student * head)
	{
		this->head = head;
	}

	Student* GetHead()
	{
		return head;
	}

	void DeleteStudent(Student* student)
	{
		for (unsigned int i = 0; i < st.size(); i++)
		{
			if (student == st[i])
			{
				st[i] = st.back();
				st.resize(st.size() - 1);
				break;
			}
		}
	}

	double AvgMark()
	{
		double avg = 0;
		for (unsigned int i = 0; i < st.size(); i++)
			avg += st[i]->GetAvgMark();
		avg /= st.size();
		return avg;
	}

};
#endif

