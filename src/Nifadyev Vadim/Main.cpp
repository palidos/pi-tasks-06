#include <iostream>
#include <clocale>
#include "Student.h"
#include "Group.h"
#include "DeansOffice.h"
using namespace std;

void DeansOfficeDemonstration(Deans_office* dof); // ���� ������ ��������
void AddStudentToNewGroupDemonstration(Student* student, Group* group, Deans_office* dof); // ���� ���������� ������ �������� � ����� ������
void ChangeGroupDemonstration(Deans_office* dof); // ���� �������� ������ �������� �� ����� ������ � ������������
void Check(Deans_office* dof); // �������� �� ��������� ���������

int main()
{
	setlocale(LC_CTYPE, "rus"); // ����� ��������� �� �������
	Deans_office* dof = new Deans_office();
	Group* group = new Group("381600"); // �������� ����� ������
	Student* student = new Student(0, "������ ���� ��������", nullptr); // �������� ������ ��������
	DeansOfficeDemonstration(dof);
	AddStudentToNewGroupDemonstration(student, group, dof);
	ChangeGroupDemonstration(dof);
	Check(dof);
	return 0;
}

void DeansOfficeDemonstration(Deans_office* dof)
{
	dof->LoadGroups(); // �������� ����� �� TXT �����
	dof->LoadStudents(); // �������� ��������� �� TXT �����
	dof->AddRandomMarks(); // ���������� ��������� ������ ���������
	dof->RandomGroupsHeads(); // ��������� ���������� �������
	dof->StudentsAverageMarks(); // ������� ������ ���������
	dof->GroupsAverageMarks(); // ������� ������ � �������
	dof->SendingDown(); // ���������� ���������
	dof->ShowHighAchievers(); // ������ �������� �����
	dof->ShowAllGroups(); // ����� ���� � �������
	dof->ShowAllStudents();// ����� ���� � ���������
	dof->SaveNewFileGroupTXT(); // ���������� ����� ������ � ������� � ����
	dof->SaveNewFileStudentTXT(); // ���������� ����� ������ � ��������� � ����
}

void AddStudentToNewGroupDemonstration(Student* student, Group* group, Deans_office* dof)
{
	dof->AddGroup(group); // ���������� ����� ������ � ������������ ������ �����
	dof->AddStudent(student); // ���������� ������ �������� � ������������ ������ ���������
	dof->AddRandomMarks(); // ���������� ������ ������ �������� � +10 ������ ���������
	dof->FindGroup("381600")->AddStudent(dof->FindStudent(0)); // ���������� ������ �������� � ����� ������
}

void ChangeGroupDemonstration(Deans_office* dof)
{
	dof->FindGroup("381600")->ShowGroupInfo(cout); // ����� ���� � ����� ������
	dof->ChangeGroup(dof->FindStudent(0), dof->FindGroup("381303")); // ������� ������ �������� � ������������ ������
}

void Check(Deans_office* dof)
{
	dof->FindGroup("381600")->ShowGroupInfo(cout); // ����� ���� � ����� ������
	dof->FindGroup("381303")->ShowGroupInfo(cout); // ����� ���� � ������������ ������
}