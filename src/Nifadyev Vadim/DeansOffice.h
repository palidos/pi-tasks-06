﻿#pragma once
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include "Student.h"
#include "Group.h"
using namespace std;

const int RANDOM_MARKS_NUMBER = 10;

class Deans_office
{
private:
	Student** students; // Массив студентов
	Group** groups; // Массив групп
	int groupsNumber;
	int studentsNumber;
public:
	Deans_office() : students(nullptr), groups(nullptr), groupsNumber(0), studentsNumber(0) {};

	~Deans_office()
	{
		for (int i = 0; i < groupsNumber; i++)
			delete groups[i];
		delete[] groups;
		for (int i = 0; i < studentsNumber; i++)
			delete students[i];
		delete[] students;
		groups = nullptr;
		students = nullptr;
	}

	// Методы

	void LoadGroups() // Загрузка инфо о группах
	{
		ifstream fileGroups("..\\Dean's office\\groups.txt");
		string title = "";
		if (fileGroups.is_open() == false) // Если файл не найден
		{
			cout << "File cannot be opened" << endl;
			exit(EXIT_FAILURE);
		}
		while (fileGroups >> title)
			AddGroup(new Group(title));
		for (int i = 0; i < studentsNumber; i++)
				students[i]->GetGroup()->AddStudent(students[i]);
		fileGroups.close();
	}

	void LoadStudents() // Загрузка инфо о студентах
	{
		ifstream fileStudents("..\\Dean's office\\students.txt");
		if (fileStudents.is_open() == false)
		{
			cout << "File \"students.txt\" cannot be opened" << endl;
			exit(EXIT_FAILURE);
		}
		while (fileStudents.eof() == false) // Пока файл не закончился
		{
			int id = 0;
			string fio = "", surname = "", name = "", patronymic = "";
			string title = "";
			fileStudents >> id;
				fileStudents >> surname >> name >> patronymic;
				fio = surname + " " + name + " " + patronymic;
				fileStudents >> title;
				for(int i = 0; i < groupsNumber; i++)
					if (title == groups[i]->GetTitle())
					{
						Student* st = new Student(id, fio, groups[i]); // Необходимо для добавления студентов в группы
						AddStudent(st);
						groups[i]->AddStudent(st);
					}
		}
		fileStudents.close();
	}

	void AddGroup(Group* group) // Добавление группы
	{
		if (groupsNumber == 0)
		{
			groups = new Group*[1];
			groups[groupsNumber] = group;
		}
		else
		{
			Group** tmp = new Group*[groupsNumber + 1];
			for (int i = 0; i < groupsNumber; i++)
				tmp[i] = groups[i];
			delete[] groups;
			groups = tmp;
			groups[groupsNumber] = group;
		}
		groupsNumber++;
	}

	void AddStudent(Student* student) // Добавление студента
	{
		if (studentsNumber == 0)
		{
			students = new Student*[1];
			students[studentsNumber] = student;
		}
		else
		{
			Student** tmp = new Student*[studentsNumber + 1];
			for (int i = 0; i < studentsNumber; i++)
				tmp[i] = students[i];
			delete[] students;
			students = tmp;
			students[studentsNumber] = student;
		}
		studentsNumber++;
	}

	Student* FindStudent(int id) // Поиск студента по ID 
	{
		int i = 0;
		if (studentsNumber == 0)
		{
			cout << "There are no students in any group" << endl;
			return nullptr;
		}
		else
		{
			for (i = 0; i < studentsNumber; i++)
				if (students[i]->GetID() == id)
					return students[i];
			if (studentsNumber == i)
			{
				cout << "There isn't any student with this ID in these groups" << endl;
				return nullptr;
			}
		}
	}

	Group* FindGroup(string title) // Поиск группы по ее имени
	{
		int i = 0;
		if (groupsNumber == 0)
		{
			cout << "This group didn't found" << endl;
			return nullptr;
		}
		else
		{
			for (int i = 0; i < groupsNumber; i++)
				if(groups[i]->GetTitle() == title)
					return groups[i];
			if (groupsNumber == i)
			{
				cout << "The group with this title doesn't exist" << endl;
				return nullptr;
			}
		}
	}

	void AddRandomMarks() // Добавление студентам случайных оценок
	{
		srand(time(NULL));
		for (int i = 0; i < studentsNumber; i++)
			for (int j = 0; j < RANDOM_MARKS_NUMBER; j++)
				students[i]->AddMark(rand() % 4 + 2); // Оценка от 2 до 5
	}

	void ChangeGroup(Student* student, Group* newGroup) // Перевод студента в другую группу
	{
		if (student == nullptr || newGroup == nullptr)
		{
			cout << "Input error!" << endl;
			return;
		}
		if (student->GetGroup() == nullptr)
			return;
		student->GetGroup()->SendingDown(student->GetID());
		newGroup->AddStudent(student);
	}

	void GroupsAverageMarks() // Средние оценки групп
	{
		float maxAverageMark = 0;
		Group* tmp = nullptr;
		cout << "\n\nGroup average marks: " << endl;
		for (int i = 0; i < groupsNumber; i++)
		{
			cout << "   Average mark in group " << groups[i]->GetTitle() << ": " << groups[i]->AverageMarkInGroup() << endl;
			if (groups[i]->AverageMarkInGroup() > maxAverageMark)
			{
				maxAverageMark = groups[i]->AverageMarkInGroup();
				tmp = groups[i];
			}
		}
		cout << "\nBest group: " << tmp->GetTitle() <<" with average mark " << tmp->AverageMarkInGroup() << "\n" << endl;
	}

	void StudentsAverageMarks() // Средние оценки студентов
	{
		float maxAverageMark = 0;
		Student* tmp = nullptr;
		cout << "Student average marks: " << endl;
		for (int i = 0; i < studentsNumber; i++)
		{
			cout << "   " << students[i]->GetFIO() << ": " << students[i]->AverageMark() << endl;
			if (students[i]->AverageMark() > maxAverageMark)
			{
				maxAverageMark = students[i]->AverageMark();
				tmp = students[i];
			}
		}
		cout << "\nBest student " << "  " << tmp->GetFIO() << " with average mark " << tmp->AverageMark() << "\n" << endl;
	}

	void ShowAllGroups() // Вывод инфо о всех группах
	{
		if (groupsNumber == 0)
		{
			cout << "There are't any group" << endl;
			return;
		}
		else
		{
			cout << "Groups: " << endl;
			for (int i = 0; i < groupsNumber; i++)
				groups[i]->ShowGroupInfo(cout);
		}
	}

	void ShowAllStudents() // Вывод инфо обо всех студентах
	{
		if (studentsNumber == 0)
		{
			cout << "There is any student" << endl;
			return;
		}
		else
		{
			cout << "\n\nAll students: ";
			for (int i = 0; i < studentsNumber; i++)
			{
				cout << "\n   " << students[i]->GetFIO() << "\n      ID: " << students[i]->GetID() 
					 << "\n      Group: " << students[i]->GetGroup()->GetTitle() << endl;
				cout << "      Marks: ";
				students[i]->ShowMarks(cout);
			}
 		}
	}

	void ShowHighAchievers() // Вывод инфо о лучших студентах
	{
		for (int i = 0; i < groupsNumber; i++)
			groups[i]->HighAchievers();
		cout << "\n\n\n";
	}

	void RandomGroupsHeads() // Случайное назначение старост групп
	{
		for (int i = 0; i < groupsNumber; i++)
			groups[i]->RandomHead();
	}

	void SendingDown() // Отчисление студента
	{
		int id = 0;
		Student** tmp = nullptr;
		int counter = studentsNumber;
		cout << "\n\nList of expelled students" << endl;
		for (int i = 0; i < studentsNumber; i++)
			if (students[i]->AverageMark() < 3)
			{
				cout << "   " << students[i]->GetFIO() << endl;
				id = students[i]->GetID();
				students[i]->GetGroup()->SendingDown(id);
				delete students[i];
				students[i] = nullptr;
				counter--;
			}
		tmp = new Student*[counter];
		counter = 0;
		for (int i = 0; i < studentsNumber; i++)
			if (students[i])
			{
				tmp[counter] = students[i];
				counter++;
			}
		delete[] students;
		students = tmp;
		studentsNumber = counter;
		cout << "\n" << endl;
 	}

	void SaveNewFileStudentTXT() // Сохранение новых данных в TXT файл
	{
		ofstream output("..\\Dean's office\\new students.txt");
		if (output.is_open() == false)
		{
			cout << "Error! Couldn't create a file" << endl;
			exit(EXIT_FAILURE);
		}
		output << "All students(without expelled students): " << endl;
		for (int i = 0; i < studentsNumber; i++)
			output << students[i]->GetID() << " " << students[i]->GetFIO() << endl;
		output.close();
	}

	void SaveNewFileGroupTXT() // Сохранение новых данных о группах в TXT файл
	{
		ofstream output("..\\Dean's office\\new groups.txt");
		if (output.is_open() == false)
		{
			cout << "Error! Couldn't create a file" << endl;
			exit(EXIT_FAILURE);
		}
		output << "All groups: " << endl;
		for (int i = 0; i < groupsNumber; i++)
			groups[i]->ShowGroupInfo(output);
		output.close();

	}
};