#pragma once
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

class Group;
class Student
{
private:
	int id;
	string fio;
	Group *group;
	int* marks; // ������ ������
	int marksNumber; // ���������� ������
public:
	Student() : fio(""), id(0), group(nullptr), marks(nullptr), marksNumber(0) {};

	Student(int id, string fio, Group* group)
	{
		this->id = id;
		this->fio = fio;
		this->group = group;
		marks = nullptr;
		marksNumber = 0;
	}

	Student(const Student &student)
	{
		id = student.id;
		fio = student.fio;
		group = student.group;
		marksNumber = student.marksNumber;
		if (marksNumber > 0)
		{
			marks = new int[marksNumber];
			for (int i = 0; i < marksNumber; i++)
				marks[i] = student.marks[i];
		}
	}

	~Student()
	{
		delete[] marks;
		marks = nullptr;
	}

	//������

	void SetID(int id) { this->id = id; }

	int GetID() const { return id; }

	void setFIO(string fio) { this->fio = fio; }

	string GetFIO() const { return fio; }

	void SetGroup(Group *group) { this->group = group; }

	Group* GetGroup() { return group; }

	void AddMark(int mark) // ���������� ������
	{
		if (marksNumber == 0)
		{
			marks = new int[1];
			marks[marksNumber] = mark;
		}
		else
		{
			int *tmp = new int[marksNumber + 1];
			for (int i = 0; i < marksNumber; i++)
				tmp[i] = marks[i];
			tmp[marksNumber] = mark;
			delete[] marks;
			marks = tmp;
		}
		marksNumber++;
	}

	float AverageMark() // ���������� ������� ������
	{
		float averageMark = 0;
		if (marksNumber == 0)
			return 0;
		else
		{
			for (int i = 0; i < marksNumber; i++)
				averageMark += marks[i];
				return (averageMark / marksNumber);
		}
	}

	void ShowMarks(ostream& output) // ����� ������
	{
		if (marksNumber == 0)
		{
			output << "This student has got no marks" << endl;
			return;
		}
		else
		{
			for (int i = 0; i < marksNumber; i++)
				output << marks[i] << " ";
			output << endl;
		}
	}
};