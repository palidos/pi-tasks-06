#pragma once
#include <iostream>
#include <ctime>
#include "Student.h"

using namespace std;

class Group
{
private:
	Student** students; // ��������
	Student* head; // ��������
	string title; // �������� ������
	int studentsNumber; // ���������� ���������
public:
	Group() : students(nullptr), head(nullptr), title(""), studentsNumber(0)  {};

	explicit Group(string title)
	{
		students = nullptr;
		head = nullptr;
		this->title = title;
		studentsNumber = 0;
	}

	Group(const Group &group)
	{
		title = group.title;
		studentsNumber = group.studentsNumber;
		if (studentsNumber != 0)
		{
			students = new Student*[studentsNumber];
			for (int i = 0; i < studentsNumber; i++)
				students[i] = group.students[i];
			head = group.head;
		}
	}

	~Group()
	{
		for (int i = 0; i < studentsNumber; i++)
			delete[] students[i];
		delete[] students;
		students = nullptr;
	}

	//������
	
	void SetTitle(string title) { this->title = title; }

	string GetTitle() const { return title; }

	void AddStudent(Student* student) // ���������� �������� � ������
	{
		if (studentsNumber == 0)
		{
			students = new Student*[1];
			students[studentsNumber] = student;
		}
		else
		{
			Student** tmp = new Student*[studentsNumber + 1];
			for (int i = 0; i < studentsNumber; i++)
				tmp[i] = students[i];
			delete[] students;
			students = tmp;
			students[studentsNumber] = student;
		}
		student->SetGroup(this);
		studentsNumber++;
	}
	 
	void ChooseHead(int id) { head = FindStudent(id); } // ����� ��������

	void RandomHead() // ��������� ���������� ��������
	{
		srand(time(NULL));
		if (studentsNumber)
			head = students[rand() % studentsNumber];
	}
	
	Student* GetHead()
	{
		if (head == nullptr)
		{
			cout << "The head of the group wasn't chosen" << endl;
			return nullptr;
		}
		else
			return head;
	}

	Student* FindStudent(int id) // ����� �������� �� ID
	{
		int i = 0;
		if (studentsNumber == 0)
		{
			cout << "Student is not a member of group " << title << endl;
			return nullptr;
		}
		else
		{
			for (i = 0; i < studentsNumber; i++)
				if (students[i]->GetID() == id)
					return students[i];
			if (i == studentsNumber)
				return nullptr;
		}
	}

	float AverageMarkInGroup() // ������� ������
	{
		float averageMark = 0;
		if (studentsNumber == 0)
		{
			cout << "The group " << title << " doesn't have students" << endl;
			return 0;
		}
		else
		{
			for (int i = 0; i < studentsNumber; i++)
				averageMark += students[i]->AverageMark();
			return (averageMark / studentsNumber);
		}
	}

	void SendingDown(int id) // ���������� ��������
	{
		int i = 0;
		for (i; i < studentsNumber; i++)
			if (students[i]->GetID() == id)
			{
				if (head == students[i])
					head = nullptr;
				if (students[i] != students[studentsNumber - 1])
					students[i] = students[studentsNumber - 1];
				students[studentsNumber - 1] = nullptr;
				studentsNumber--;
				break;
			}
	}

	void HighAchievers() // ����� ��������� �� ������� ������ ���� 4
	{
		if (studentsNumber == 0)
		{
			cout << "The group " << title << " is empty" << endl;
			return;
		}
		else
		{
			bool coincidence = false; 
			cout << "\nStudents with highest marks in group " << title << ": " << endl;
			for (int i = 0; i < studentsNumber; i++)
				if (students[i]->AverageMark() >= 4)
				{
					coincidence = true;
					cout << "   " << students[i]->GetFIO() << " : " << students[i]->AverageMark() << endl;
				}
			if (coincidence == false)
			{
				cout << "The group " << title << " doesn't have excellent students" << endl;
				cout << "Nobody doesn't have stipend" << endl;
			}
		}
	}

	void ShowGroupInfo(ostream& output) // ����� ���� �� �������
	{
		output << "\nGroup " << title << endl;
		if (studentsNumber == 0)
		{
			cout << "   This group doesn't have students\n" << endl;
			return;
		}
		else
		{
			output << "   Students:" << endl;
			for (int i = 0; i < studentsNumber; i++)
			{
				output << "      " << students[i]->GetFIO() << "\n\tID: " << students[i]->GetID() << "\n\tMarks: ";
				students[i]->ShowMarks(output);
				output  <<"\tAverage mark : " << students[i]->AverageMark() << "\n" << endl;
			}
			if (head == nullptr)
				output << "The head of the group hasn't been chosen" << endl;
			else
				output << "The head of the group: " << GetHead()->GetFIO() << endl;
			output << "\nAverage mark in the group: " << AverageMarkInGroup() << "\n" << endl;
		}
	}
};