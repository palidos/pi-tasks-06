// �������� ��� ����-������ ��������� "�������".

#include "Group.h"
#include "Student.h"
#include "DeansOffice.h"
#include <iostream>
using namespace std;
int main()
{
	DeansOffice *dnOff = new DeansOffice();	// �������� ������� ������ "�������".
	dnOff->loadGroups();	// �������� ������ ����� �� �����.
	dnOff->loadStudents();	// �������� ������ ��������� �� �����.
	dnOff->distributeStudents();	// ��������� ������������� ��������� �� �������.
	dnOff->randomMarks();	// ����������� 10 �������� ������ ������� ��������.
	dnOff->StudentsAvgMarks(); // ����� �� ������� ���������� � ������� ������������ ���������. 
	dnOff->sendDown();	// ���������� �������� �� ��������������.
	dnOff->showStudents();	// ����� �� ������� ������ ������������� ���������.
	dnOff->randomGroupsHeads();	// ���������� ��������� ������� � ������.
	dnOff->showGroups();	// ����� �� ������� ������� �������������� �����.
	dnOff->groupsAvgMarks();	// ������������ �����. ����� ���������� � ������ ������.
	dnOff->saveNewStudentsList();	// ���������� ������������� ��������� � ����.
	dnOff->saveNewGroupsList();		// ���������� ���������� � ������� � ����.


	// ������ ������ "������� �������� � ������ ������".

	Group *gr = new Group("000000-0 Custom group");	// �������� ����� ������.
	Student *st = new Student(0, "Custom student");	// �������� ������ ��������.
	dnOff->addGroup(gr);	// ���������� ��������� ������ � ����� ������ �����.
	dnOff->addStudent(st); // ���������� ���������� �������� � ����� ������ ���������.
	dnOff->findGroup("000000-0 Custom group")->addStudent(dnOff->findStudent(0)); // ���������� ���������� �������� 
																				  // � ��������� ������.

	dnOff->findGroup("000000-0 Custom group")->showGroupInfo(cout); // ����� ���������� � ��������� ������.
	dnOff->replaceStudent(dnOff->findStudent(0), dnOff->findGroup("381608-1 SWE")); // ������� ���������� �������� � ������ ������.

	// ��������.
	dnOff->findGroup("000000-0 Custom group")->showGroupInfo(cout);
	dnOff->findGroup("381608-1 SWE")->showGroupInfo(cout);
	return 0;
}