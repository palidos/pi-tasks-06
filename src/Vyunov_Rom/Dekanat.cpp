#include "Student.h"
#include "Group.h"
#include "Dekanat.h"

void Dekanat::createGroupList(string Path)
{
	ifstream in;
	in.open(Path);
	if (!in)
	{
		cout << "�� �������� ������� ����!" << endl;
		return;
	}
	string Title;
	while(in >> Title)
		createGroup(Title);
	in.close();
}
void Dekanat::createGroup(string Title)
{
	Group ** tmp = new Group *[GroupCount + 1];
	for (int i = 0; i < GroupCount; i++)
	{
		tmp[i] = gr[i];
	}
	delete[] gr;
	gr = tmp;
	Group * newGroup = new Group(Title);
	gr[GroupCount] = newGroup;
	cout << "������� ����� ������: " << newGroup->getTitle() << endl;
	GroupCount++;
}

void Dekanat::createStud(int ID, string FIO, string GroupTitle)
{
	Student ** tmp = new Student *[StudCount + 1];
	for (int i = 0; i < StudCount; i++)
	{
		tmp[i] = Students[i];
	}
	delete[] Students;
	Students = tmp;
	Student * newStud = new Student(ID,FIO);
	Group * tmpGr= findGroup(GroupTitle);
	newStud->setGroup(tmpGr);
	tmpGr->addStudent(newStud);

	Students[StudCount] = newStud;
	cout << "�������� ����� �������: " << newStud->getFIO() <<"\t"<<newStud->getGroup()->getTitle()<< endl;
	StudCount++;
}
void Dekanat::createStudList(string Path)
{
	ifstream in;
	in.open(Path);
	if (!in)
	{
		cout << "�� �������� ������� ����!" << endl;
		return;
	}
	string GroupTitle,FIO,buf;
	int ID;
	do
	{
		ID = 0;
		buf = "";
		FIO = "";
		GroupTitle = "";
		in >> ID;
		in >> buf;
		FIO += buf + " ";
		in >> buf;
		FIO += buf + " ";
		in >> buf;
		FIO += buf;
		in >> GroupTitle;
		createStud(ID, FIO, GroupTitle);
	} while (in.get() != EOF);
	in.close();
}

void Dekanat::addRNDMarkForAllStud()
{
	cout << endl;
	for (int i = 0; i < StudCount; i++)
	{
		cout << "��������� ������ �������� " << Students[i]->getFIO() << " : ";
		int MarksCount = rand() % 11 + 10; //���������� �� 10 �� 20 ������
		for (int j = 0; j < MarksCount; j++)
		{
			int Mark = rand() % 5 + 1;
			Students[i]->addMark(Mark);//���������� ������ �� 1 �� 5
			cout << Mark;
		}
		cout << endl;
	}
	cout <<endl<< "~~~~~~~~~������ ��������� ���� ���������!~~~~~~~~~~~~~~~~~~" <<endl<< endl;
}

void Dekanat::transferStud(Student * Stud, Group * toGroup)
{
	cout << "������� " << Stud->getFIO() << " ��������� �� " << Stud->getGroup()->getTitle();
	Stud->getGroup()->excludeStud(Stud);
	Stud->setGroup(toGroup);
	toGroup->addStudent(Stud);
	cout << " � " << toGroup->getTitle() << endl;
}

void Dekanat::excludeStud(Student * st)
{
	int st_num;
	for (int i = 0; i < StudCount; i++)
	{
		if (Students[i] == st)
		{
			st_num = i;
			break;
		}
	}
	
	st->getGroup()->excludeStud(st);
	cout << "������� " << st->getFIO() << " ��� �������� �� ������������! ";
	if (st == st->getGroup()->getHead())
	{
		cout << "�� ��� ��������� ������ " << st->getGroup()->getTitle()<<endl;
		cout << "����� �������� : ";
		choiseHead(st->getGroup());
	}
	cout << endl;
		
	Students[st_num] = Students[StudCount - 1];

	Student ** tmp = new Student *[--StudCount];
	for (int i = 0; i < StudCount; i++)
	{
		tmp[i] = Students[i];
	}
	delete[] Students;
	Students = tmp;
}

void Dekanat::checkProgress()
{
	cout << endl<< "~~~~~~~~~~�������� ����������� �� ��������������~~~~~~~~~~~~~~~~" << endl;
	for (int i = 0; i < StudCount; i++)
	{
		if (Students[i]->getAverageRating() < 3)//���� ������ ���� 4
		{
			excludeStud(Students[i]);
		}
	}
	cout <<endl<< "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" <<endl<< endl;
}

void Dekanat::choiseHead(Group * gr)
{
	int rndStud = rand() % gr->getNum();
	gr->setHead(gr->getStud(rndStud));
	cout << "������� " << gr->getHead()->getFIO() << " ������ ��������� ������ " << gr->getTitle() << endl;
}
void Dekanat::choiseHeadForAll()
{
	cout << endl << "~~~~~~~~~~~~~~~~~~��������� ������ ��������~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
	for (int i = 0; i < GroupCount; i++)
	{
		choiseHead(gr[i]);
	}
	cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
}

void Dekanat::AvarageGroupRating()
{
	cout << endl << "~~~~~~~~~������� ���� � �������~~~~~~~~~~~~~~~~~~~~" << endl << endl;
	for (int i = 0; i < GroupCount; i++)
	{
		cout << gr[i]->getTitle() <<" : "<<gr[i]->getAverageRating()<< endl;
	}
	cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
}
void Dekanat::AvarageStudRating()
{
	cout << endl << "~~~~~~~~~~������� ���� ���������~~~~~~~~~~~~~~~~~~~" << endl << endl;
	for (int i = 0; i < StudCount; i++)
	{
		cout << Students[i]->getID() << "\t" << Students[i]->getFIO() << "\t" << " : " <<
			Students[i]->getAverageRating() << endl;
	}
	cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
}

Group * Dekanat::findGroup(string Title)
{
	for (int i = 0; i < GroupCount; i++)
	{
		if (Title == gr[i]->getTitle())
		{
			return gr[i];
		}
	}

	return nullptr;
}
Student * Dekanat::findStud(int ID)
{
	for (int i = 0; i < StudCount; i++)
	{
		if (ID == Students[i]->getID())
		{
			return Students[i];
		}
	}

	return nullptr;
}
Student * Dekanat::findStud(string FIO)
{
	for (int i = 0; i < StudCount; i++)
	{
		if (FIO == Students[i]->getFIO())
		{
			return Students[i];
		}
	}

	return nullptr;
}

Student * Dekanat::rndStud()
{
	return Students[rand() % StudCount];
}
Group * Dekanat::rndGroup()
{
	return gr[rand() % GroupCount];
}